# redis-limiter

#### 介绍
采用 redis + Lua + Spring AOP实现高性能接口限流

#### 软件架构
软件架构说明


#### 使用说明

1.  📎下载源码
2.  🎨修改application.properties中的Redis配置
3.  ⛸启动项目
4.  ⚖验证限流

🎗详细说明请参考：https://mp.weixin.qq.com/s/p8Eb092RCXONENtkhuqe3A

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

