package com.biggerboy.redislimiter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisLimiterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisLimiterApplication.class, args);
    }

}
