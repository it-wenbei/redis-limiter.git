package com.biggerboy.redislimiter.enums;

/**
 * 限流类型（维度）.
 *
 * @author : 北哥 公众号：BiggerBoy
 * @version : 1.0 2022/09/09
 * @since : 1.0
 */
public enum LimitType {
    IP,
    CUSTOMER;
}
